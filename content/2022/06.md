---
title: "June"
weight: 7
---

# Arch Linux in June 2022

## devtools

The new devtools release brings in several new additions. First and
foremost the `export-pkgbuild-keys` script in combination with
`commitpkg` ensures we export all validpgpkeys into the source tree. The
local keys cache will allow us to operate without remote PGP keyservers
for the most part [0].

Furthermore the new `diffpkg` tool allows to automatically diff a
package file against the current repository version or another package
file. Multiple diff modes allow comparing archive listings, the whole
content via diffoscope or the .PKGINFO and .BUILDINFO metadata [1].

Last but not least we have added `x86_64_v3` builders as a first step
towards more optimized architectures. The scripts and configs can be
used for testing purposes [2].


## archlinux.page

In cooperation with SPI and Gandi a new domain has been acquired to host
the documentation of projects related to Arch Linux:
https://archlinux.page

First deployments are already available on the following subdomains:

- https://repod.archlinux.page/
- https://monthly-reports.archlinux.page/


## archiso

The official installation ISO image can now boot on IA32 UEFI [3].


## SPI

The annual SPI report for member projects updates has been submitted and
will be available shortly through SPI.


## glibc

The glibc package ships with the `C.UTF-8` locale available by default.


[0] https://gitlab.archlinux.org/archlinux/devtools/-/merge_requests/98  
[1] https://gitlab.archlinux.org/archlinux/devtools/-/merge_requests/99  
[2] https://gitlab.archlinux.org/archlinux/devtools/-/merge_requests/86  
[3] https://gitlab.archlinux.org/archlinux/archiso/-/merge_requests/216
