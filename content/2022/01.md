---
title: "January"
weight: 12
---

# Arch Linux in January 2022

## Staff

We have on-boarded wiki maintainers as part of our official staff [0] and wish a warm welcoming.

We would also like to welcome BrainDamage among the Arch IRC op team.


## Debug packages

The required devtools release is live, which handles debug packages and
makes sure to release them when available.

Currently we have a debuginfod service which is capable of delivering
source listings and debug symbols to users with gdb, delve and other
debuggers. This can be enabled by installing the `debuginfod` package
and setting the environment variable
`DEBUGINFOD_URLS="https://debuginfod.archlinux.org"`. The
`debuginfod` package is also going to be providing this variable.


## devtools

The devtools project has migrated to GitLab as well including all
currently open merge requests and issues. Unfortunately the migrated
merge requests are just a branch, in case the original authors want to
update them, please resubmit the patches.


## Keyring

Our new keyring tooling has completely replaced the old usage and data
storage. We have merged the new curated keyring data and released the
first few keyrings using the new curated tooling and data.

Furthermore we have added more packager keys with @archlinux.org uids so
we can make sure all important keys will be reachable through our own
WKD.


## Gluebuddy

A new helper tool named gluebuddy has been released [1] and successfully
rolled out. Gluebuddy helps devops to maintain certain aspects of our on
and off-boarding with automation by syncing GitLab groups and project
settings. This reduces the required manual work and ensures we have
quicker on-boarding as well as do never forget to remove permissions for
former staff.


## linux-firmware

The linux-firmware package has been split up to reduce download and
installation size for most setups.


## OpenSearch

We have replaced the elasticsearch package stack with OpenSearch. On top
we have started [working towards adding Arch Linux to the official
OpenSearch download page[2]. Furthermore some progress has been made to
allow reproducible builds[3]


[0] https://gitlab.archlinux.org/archlinux/infrastructure/-/issues/375  
[1] https://gitlab.archlinux.org/archlinux/gluebuddy  
[2] https://github.com/opensearch-project/project-website/pull/642  
[3] https://github.com/opensearch-project/OpenSearch/pull/1995
