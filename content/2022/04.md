---
title: "April"
weight: 9
---

# Arch Linux in April 2022

## Keyring

We have started phasing out sha1 keyring signatures and replace some existing keys with more modern variants.

Furthermore we have started to enforce @archlinux.org UIDs so we can
control the retrieval via wkd in a single place and also allow externals
to just trust one official wkd domain.


## Security Team

We would like to welcome djerun and raa who will help us in the security
team to keep track of all vulnerabilities.


## Wiki

We would like to welcome Erus Iluvatar as an ArchWiki Maintainer and
Translator [0].


## Infrastructure

We now have a GeoIP based mirror https://geo.mirror.pkgbuild.com for
docker images and arch-boxes. After a MR lands in osinfo-db this will
allow users on all distro's with an update osinfo-db to use gnome-boxes
to install Arch Linux with the "download operating system" option.


[0] https://wiki.archlinux.org/title/ArchWiki:News#2022/04/13_-_New_maintainer_and_translator  
[1] https://gitlab.com/libosinfo/osinfo-db/-/merge_requests/444
