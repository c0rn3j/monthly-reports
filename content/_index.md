---
type: docs
---

This page contains Arch Linux monthly reports.

Git repository: https://gitlab.archlinux.org/archlinux/monthly-reports
